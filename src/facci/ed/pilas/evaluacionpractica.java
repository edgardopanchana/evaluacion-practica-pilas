
package facci.ed.pilas;


import java.io.*;
import java.util.Random;
import javax.swing.JOptionPane;

public class evaluacionpractica {
//Longitud maxima de la Pila
   public static final int MAX_LENGTH = 10;
   //Array que representa a la pila principal
   public static String Pila[] = new String[MAX_LENGTH];
   //Cima de la pila principal
   public static int cima = -1;
   //Array que representa a la pila auxiliar
   public static String Pilaaux[] = new String[MAX_LENGTH];
   //Cima de la pila auxiliar
   public static int cimaaux = -1;
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args)throws IOException{
        // TODO code application logic here
        LlenarPila();
        Menu();
    }
    
    public static void Menu()throws IOException{
       String salida="====Menú Manejo Pila====\n"+"1- Insertar elemento\n"+"2- Eliminar elemento\n";
       salida=salida+"3- Imprimir pila\n";
       salida=salida+"4- Salir\n";
       String entra=JOptionPane.showInputDialog(null, salida);
       int op = Integer.parseInt(entra);
       Opciones(op);
    }

    public static void Opciones(int op)throws IOException{
        String salio;
        switch(op){
			case 1: Insertar();
                                Menu();
			        break;
			case 2: salio=Pop();
                                if (!Vacia()){
                                   JOptionPane.showMessageDialog(null, "El dato que salio es "+salio); 
                                }
                                Menu();
			        break;
			case 3: Imprimir();
                                Menu();
			        break;
                         
			case 4: System.exit(0);
			        break;
			default:Menu();
			        break;
                        
	   }
    }

    public static void Insertar()throws IOException{
       String entra = JOptionPane.showInputDialog("Digite un dato para la pila");
       Push(entra);
    }

    public static void Push(String dato)throws IOException{
      if (PilaLlena()){
        JOptionPane.showMessageDialog(null,"Capacidad de la pila al limite");
        //Imprimir();
      }else{
            cima++;
            Pila[cima]=dato;
      }
    }

    public static void PushAux(String dato)throws IOException{
      if (PilaLlenaAux()){
        JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar al limite");
      }else{
         cimaaux++;
         Pilaaux[cimaaux]=dato;
       }
    }

    public static boolean VaciAaux(){
        return (cimaaux==-1);
    }

    public static boolean Vacia(){
        if (cima==-1){
            return (true);
        }
        else {
            return(false);
        }
    }

    public static void Imprimir()throws IOException{
      String quedata,salida=" ";
      if (!Vacia())
      { 
          
          do {
            quedata = Pop();
            salida=salida+quedata+"\n"; //System.out.println mostrando
            PushAux(quedata);            
        }while(!Vacia());
        do {
            quedata=PopAux();
            Push(quedata);
        }while(!VaciAaux());
        JOptionPane.showMessageDialog(null, salida);
      }
      else {
          JOptionPane.showMessageDialog(null, "La pila esta vacía");
      }
    }
   
    public static String Pop()throws IOException{
      String quedato;
      if(Vacia()){
          JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía !!!" );
          return("");
      }else{
              quedato=Pila[cima];
	      Pila[cima] = null;
	      --cima;
              return(quedato);
            }
    }

    public static String PopAux()throws IOException{
      String quedato;
      if(VaciAaux()){
            JOptionPane.showMessageDialog(null,"No se puede eliminar, pila vacía !!!" );
            return("");
      }else{
              quedato=Pilaaux[cimaaux];
	      Pilaaux[cimaaux] = null;
	      --cimaaux;
              return(quedato);
           }
    }

    private static boolean PilaLlena()
    {
        if ((Pila.length-1)==cima)
            return true;
        else
            return false;
    }
    
    private static boolean PilaLlenaAux()
    {
        if ((Pilaaux.length-1)==cimaaux)
            return true;
        else
            return false;
    }
    
    private static void LlenarPila() throws IOException
    {
        Random random= new Random();
        int numero;
        do {
           numero = random.nextInt((50 - 1) + 1 ) +1;
                Push(String.valueOf(numero));
            
        }while(!PilaLlena());
    }
}